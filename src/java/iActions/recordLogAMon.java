package iActions;

import java.util.LinkedList;
import java.util.List;
import java.io.FileWriter;
import java.io.PrintWriter;

import jason.asSemantics.*;
import jason.asSyntax.*;

public class recordLogAMon extends DefaultInternalAction
{
	private static final long serialVersionUID = 1L;
	
	private List<String> logAMon = new LinkedList<String>();
	
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception
    {    
		StringTerm cenario 			= (StringTerm) args[0];
    	StringTerm k 				= (StringTerm) args[1];
    	StringTerm label 			= (StringTerm) args[2];
    	StringTerm estimedTime 		= (StringTerm) args[3];
    	StringTerm currentTime 		= (StringTerm) args[4];
    	StringTerm estimedCost 		= (StringTerm) args[5];
    	StringTerm currentCost 		= (StringTerm) args[6];
    	StringTerm plannedValue		= (StringTerm) args[7];
    	StringTerm aggregateValue 	= (StringTerm) args[8];
    	StringTerm tes				= (StringTerm) args[9];
    	StringTerm tef				= (StringTerm) args[10];
    	StringTerm tls				= (StringTerm) args[11];
    	StringTerm tlf				= (StringTerm) args[12];
    	StringTerm gap				= (StringTerm) args[13];
    	
    	StringTerm variationPeriod 			= (StringTerm) args[14];
    	StringTerm realVariationCost 		= (StringTerm) args[15];
    	StringTerm costPerformanceIndex 	= (StringTerm) args[16];
    	StringTerm schedulePerformanceIndex = (StringTerm) args[17];
    	
    	StringTerm rule1 = (StringTerm) args[18];
    	StringTerm rule2 = (StringTerm) args[19];
    	StringTerm rule3 = (StringTerm) args[20];
    	StringTerm rule4 = (StringTerm) args[21];
    	
    	logAMon.add("-----------------\n");
    	logAMon.add("| Instante k: " + k + " |\n");
    	logAMon.add("-----------------\n");
		logAMon.add("Atividade: " + label + "\n");
		logAMon.add("Tempo estimado: " + estimedTime + "\n");
		logAMon.add("Tempo atual: " + currentTime + "\n");
		logAMon.add("Tempo de inicio cedo(Tes): " + tes + "\n");
		logAMon.add("Tempo de inicio tarde(Tef): " + tef + "\n");
		logAMon.add("Tempo de termino tarde(Tlf): " + tlf + "\n");
		logAMon.add("Tempo de termino cedo(Tls): " + tls + "\n");
		logAMon.add("Folga(GAP): " + gap + "\n");
		logAMon.add("Custo estimado: " + estimedCost + "\n");
		logAMon.add("Custo atual: " + currentCost + "\n\n");
		logAMon.add("---------- Estado Interno do AMon ----------\n");
		logAMon.add("Valor Planejado: " + plannedValue + "\n");
		logAMon.add("Valor Agregado: " + aggregateValue + "\n");
		logAMon.add("Variação do período: " + variationPeriod + "\n");
		logAMon.add("Variação do custo real: " + realVariationCost + "\n");
		logAMon.add("Índice de performance do custo: " + costPerformanceIndex + "\n");
		logAMon.add("Índice de performance agendada: " + schedulePerformanceIndex + "\n\n");
		logAMon.add("---------- Mensagens Preventivas ----------\n");
		logAMon.add(rule1 + "\n");
		logAMon.add(rule2 + "\n");
		logAMon.add(rule3 + "\n");
		logAMon.add(rule4 + "\n\n");
		
		/* Gravando LogAMon */
		
		FileWriter file = new FileWriter("./logs/logAMon_" + cenario.getString() + ".txt");
		PrintWriter record = new PrintWriter(file);
		
		for (int i = 0; i < logAMon.size(); i++)
			record.print(logAMon.get(i));
		
		file.close();
		
        return true;
    }
}
