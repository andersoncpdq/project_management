package models;

import java.util.ArrayList;

public class Project
{	
	private int id;
	private int duration;
	private int realDuration;
	private float budget;
	private int instant;
	
	private ArrayList<Activity> activities = new ArrayList<Activity>();
	
	public Project()
	{
		id		 	= 0;
		duration 	= 0;
		budget		= 0.0f;
		instant 	= 0;
	}
	
	public Project(int id, int duration, float budget)
	{
		this.id			= id;
		this.duration	= duration;
		this.budget		= budget;
		this.realDuration = this.duration;
	}
	
	public void clearActivities()
	{
		activities.clear();
	}
	
	public void addActivity(Activity a)
	{
		activities.add(a);
	}
	
	public void addRealDuration(int time)
	{
		this.realDuration += time;
	}
	
	public boolean existsTaskNotFinalized(int instant)
	{           //verifica se todas as tarefas acabaram
		for (Activity a: activities)
			if (a.getRealTime() > instant)
				return true;
		return false;
	}
	
	/**********************************************
						GETERS
	 **********************************************/
	
	public int getNumActivitiesRunning()
	{
		int cnt = 0;
		for (int i = 0; i < activities.size(); i++)
		{
			if(activities.get(i).isRunning(instant))
				cnt++;
		}
		return cnt;
	}

	public int[] getIdsActivitiesRunning()
	{
		int numActivitiesRunning = getNumActivitiesRunning();
		int [] ids = new int[numActivitiesRunning];
		int cnt = 0;
		
		for (Activity a : activities)
		{
			if(a.isRunning(instant))
				ids[cnt++] = a.getId();
		}
		return ids;
	}
	
	public Activity getActivityById(int id)
	{	
		Activity activity = new Activity();
		
		for (int i = 0; i < activities.size(); i++)
		{
			if(activities.get(i).getId() == id)
				activity = activities.get(i);
		}
		return activity;
	}
	
	public Object[] getActivitiesByInstant()
	{
		ArrayList<Activity> activs = new ArrayList<Activity>();
		for (Activity a : activities)
		{
			if(a.isRunning(instant))
				activs.add(a);
		}
		return activs.toArray(new Activity[]{});
	}
	
	public int getId()
	{
		return id;
	}
	
	public int getDuration()
	{
		return duration;
	}
	
	public int getRealDuration()
	{
		return realDuration;
	}

	public float getBudget()
	{
		return budget;
	}
	
	public int getInstant()
	{
		return instant;
	}	
	
	public ArrayList<Activity> getActivities()
	{
		return activities;
	}

	/**********************************************
						SETERS
	 **********************************************/

	public void setInstant(int instant)
	{
		this.instant = instant;
	}

	public void setActivities(ArrayList<Activity> activities) {
		this.activities = activities;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public void setBudget(float budget) {
		this.budget = budget;
	}
}